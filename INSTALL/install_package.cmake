# include(GNUInstallDirs) must be at the top to ensure standard install directories are defined
include(GNUInstallDirs)

# Install SQLite3 library
install(TARGETS SQLite3
        EXPORT ${PROJECT_NAME}Targets
        LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
        ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
        COMPONENT Libraries)

# Install SQLite3 header files
install(FILES ../include/sqlite3.h ../include/sqlite3ext.h DESTINATION ${CMAKE_INSTALL_INCLUDEDIR} COMPONENT headers)

# Set up packaging variables
set(CPACK_PACKAGE_NAME "SQLite3Lib")
set(CPACK_PACKAGE_VENDOR "JD")
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "SQLite3Lib - SQLite3 Library Installer")
set(CPACK_PACKAGE_VERSION_MAJOR "1")
set(CPACK_PACKAGE_VERSION_MINOR "0")
set(CPACK_PACKAGE_VERSION_PATCH "0")

# Include CPack for packaging
include(CPack)
